export MODULEPATH=$MODULEPATH:~/.local/easybuild/modules/all
ml PETSc/3.14.4-foss-2020b
for FILE  in *.txt
do
  BASE="${FILE%%.*}"
  awk -v PATTERN="$BASE-%d.tmp" 'BEGIN {n=1; FILE=sprintf(PATTERN, n)} !NF {n++; FILE=sprintf(PATTERN, n); next} {print > FILE}' $FILE 
  #awk -v RS=  -v PATTERN="$BASE-%d.tmp" '{FILE=sprintf(PATTERN, NR); print > FILE}'  $FILE
  COUNT=$(head -n1 $BASE-1.tmp | awk '{print $1}')
  sed -i '1d' $BASE-1.tmp
  python convert.py -i $BASE-1.tmp -o $BASE.bin -c $COUNT
  rm *.tmp
done
