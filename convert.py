from numpy import fromfile
from scipy.sparse import coo_matrix
import PetscBinaryIO
import sys, getopt

inputfile = ''
outputfile = ''
try:
   opts, args = getopt.getopt(sys.argv[1:],"hi:o:c:",["ifile=","ofile=","count="])
except getopt.GetoptError:
   print('test.py -i <inputfile> -o <outputfile> -c <count>')
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print('test.py -i <inputfile> -o <outputfile>')
      sys.exit()
   elif opt in ("-i", "--ifile"):
      inputfile = arg
   elif opt in ("-o", "--ofile"):
      outputfile = arg
   elif opt in ("-c", "--count"):
      count = int(arg)
print( 'Input file is ', inputfile)
print( 'Output file is ', outputfile)
print( 'Count is ', count )

IJV = fromfile(inputfile,sep=" ").reshape(-1,3)
row = IJV[:,0]
col = IJV[:,1]
data = IJV[:,2]
A = coo_matrix((data,(row,col)), shape=(count, count))
PetscBinaryIO.PetscBinaryIO().writeBinaryFile(outputfile, [A])
